%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-define(EPOCH_LENGTH, 7200). %% In blocks.
-define(MAX_BLOCK_HEIGHT, 4294967295). %% 2^32 - 1
-define(HASH(X), crypto:hash(sha256, X)).
-define(SETS, ordsets).

-type pk() :: <<_:256>>.
-type signature() :: <<_:512>>.
-type hash() :: <<_:256>>.
-type block_height() :: 1..?MAX_BLOCK_HEIGHT.
-type balance() :: 0..18446744073709551615.
-type fee() :: 0..18446744073709551615.
-type tx_value() :: 0..18446744073709551615.
-type voting_power() :: non_neg_integer().
-type message() :: binary().
-record(
   transfer_tx,
   {valid_since :: block_height(),
    from :: pk(),
    to :: pk(),
    value :: tx_value(),
    message :: message(),
    signature :: signature()}).
-type transfer_tx() :: #transfer_tx{}.
-record(
   lock_tx,
   {address :: pk(),
    locked_until :: block_height(),
    signature :: signature()}).
-type lock_tx() :: #lock_tx{}.
-record(
   account_tx,
   {from :: pk(),
    to :: pk(),
    valid_until :: block_height(),
    signature :: signature()}).
-type account_tx() :: #account_tx{}.
-type choices() :: map().
-record(
   vote,
   {valid_since :: block_height(),
    choices :: choices()}).
-type vote() :: #vote{}.
-record(
   vote_tx,
   {vote :: vote(),
    address :: pk(),
    signature :: signature()}).
-type vote_tx() :: #vote_tx{}.
-type tx() :: transfer_tx() | account_tx() | lock_tx() | vote_tx().
-record(
   account,
   {address :: pk(),
    balance=0 :: balance(),
    valid_until :: block_height(),
    locked_until=none :: block_height() | none}).
-type account() :: #account{}.
-record(
   data,
   {protocol=1 :: pos_integer(),
    epoch_length :: 2..31536000,
    height=0 :: block_height() | 0,
    fee_deposit=0 :: fee(),
    fresh_txs=?SETS:new() :: ?SETS:set(binary()),
    transfer_txs_hash= <<0:256>> :: hash(),
    vote_txs_hash= <<0:256>> :: hash(),
    accounts=gb_merkle_trees:empty() :: gb_merkle_trees:tree(),
    last_block_hash= <<0:256>> :: <<_:256>>,
    validators=gb_merkle_trees:empty() :: gb_merkle_trees:tree()}).
-type data() :: #data{}.
