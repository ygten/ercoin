# Licensed under the Apache License, Version 2.0 (the “License”);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an “AS IS” BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

PROJECT = ercoin
PROJECT_DESCRIPTION = A simple cryptocurrency using Tendermint
PROJECT_VERSION = 0.1.0

DEPS = erlsha2 libsodium abci_server gb_merkle_trees dynarec
dep_abci_server = git https://github.com/KrzysiekJ/abci_server.git
dep_dynarec = git https://github.com/dieswaytoofast/dynarec.git
dep_erlsha2 = git https://github.com/vinoski/erlsha2
dep_gb_merkle_trees = git https://github.com/KrzysiekJ/gb_merkle_trees.git
dep_jiffy = git https://github.com/davisp/jiffy.git 0.14.11
dep_libsodium = git https://github.com/potatosalad/erlang-libsodium.git

TEST_DEPS = triq
dep_triq = git https://github.com/triqng/triq.git

BUILD_DEPS = lfe lfe.mk
dep_lfe = git https://github.com/rvirding/lfe develop
dep_lfe.mk = git https://github.com/ninenines/lfe.mk master
DEP_PLUGINS = lfe.mk

# Whitespace to be used when creating files from templates.
SP = 4

include erlang.mk
