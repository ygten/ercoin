%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_genesis).

-export(
   [genesis/0,
    data_to_genesis_json/1,
    sk_to_priv_validator_json/1]).

-include_lib("include/ercoin.hrl").

hex(N) when N < 10 ->
    $0+N;
hex(N) when N >= 10, N < 16 ->
    $a+(N-10).

to_hex(N) when N < 256 ->
    [hex(N div 16), hex(N rem 16)].

-spec binary_to_iolist(binary()) -> iolist().
binary_to_iolist(<<>>) ->
    [];
binary_to_iolist(<<Head/integer, Rest/binary>>) ->
    to_hex(Head) ++ binary_to_iolist(Rest).

-spec binary_to_hex(binary()) -> binary().
binary_to_hex(Bin) ->
    iolist_to_binary(binary_to_iolist(Bin)).

-spec genesis() -> data().
genesis() ->
    {ok, GenesisBin} = file:read_file("genesis_data"),
    binary_to_term(GenesisBin).

%% @doc Convert data to a genesis.json file for Tendermint.
-spec data_to_genesis_json(data()) -> binary().
data_to_genesis_json(Genesis) ->
    Options = [pretty],
    Validators =
        [{[{<<"amount">>, Power},
           {<<"name">>, <<"">>},
           {<<"pub_key">>,
            {[{<<"type">>, <<"ed25519">>},
              {<<"data">>, binary_to_hex(PK)}]}}]}
         || {PK, <<Power/integer>>} <- gb_merkle_trees:to_orddict(Genesis#data.validators)],
    ToEncode =
        {[{<<"app_hash">>, binary_to_hex(ercoin_abci:app_hash(Genesis))},
          {<<"genesis_time">>, <<"0001-01-01T00:00:00.000Z">>},
          {<<"chain_id">>, <<"ercoin-test">>},
          {<<"validators">>, Validators}]},
    jiffy:encode(ToEncode, Options).

%% This doesn’t seem to work.
sk_to_priv_validator_json(SK) ->
    PK = libsodium_crypto_sign_ed25519:sk_to_pk(SK),
    Options = [pretty],
    ToEncode =
        {[{<<"address">>, binary_to_hex(crypto:hash(ripemd160, PK))},
          {<<"last_height">>, 0},
          {<<"last_round">>, 0},
          {<<"last_signature">>, null},
          {<<"last_signbytes">>, <<"">>},
          {<<"last_step">>, 0},
          %% Outdated format below.
          {<<"priv_key">>, [1, binary_to_hex(SK)]},
          {<<"pub_key">>, [1, binary_to_hex(PK)]}]},
    jiffy:encode(ToEncode, Options).
