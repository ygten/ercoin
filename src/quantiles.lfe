;; Licensed under the Apache License, Version 2.0 (the “License”);
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at
;;
;;     http://www.apache.org/licenses/LICENSE-2.0
;;
;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an “AS IS” BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.

(defmodule quantiles
  (export (weighted-quantile 3)))

(defspec (weighted-quantile 3)
  (((float) (list (tuple val weight)) weight-sum)
   val
   ((val (integer)) (weight (pos_integer)) (weight-sum (pos_integer)))))
(defun weighted-quantile
  ((_ (list (tuple val _)) _)
   val)
  ((degree (cons (tuple val weight) tail) weight-sum)
   ;; TODO: Test that > is used here, not >=.
   (if (> (/ weight weight-sum) degree)
     val
     (weighted-quantile (/ (- degree (/ weight weight-sum)) (/ (- weight-sum weight) weight-sum)) tail (- weight-sum weight)))))

(defmodule quantiles_test
  ;; Triq’s autoexport parse transform doesn’t work here, as parse transforms don’t work in LFE, so we just export everything.
  (export all))

(include-lib "triq/include/triq.hrl")

(defun weight-sum (l)
  (weight-sum l 0))

(defun weight-sum
  (((cons (tuple _ weight) tail) acc)
   (weight-sum tail (+ acc weight)))
  (([] acc)
   acc))

(defun gen_map (key_type val_type)
  (LET
   proplist
   (list (tuple key_type val_type))
   (maps:from_list proplist)))

(defun weight ()
  (pos_integer))

(defun degree ()
  (LET
   f
   (real)
   (abs (- f (trunc f)))))

(defun weighted-int-list ()
  (LET
   l
   (non_empty (triq_dom:list (tuple (int) (weight))))
   (lists:sort l)))

(defun prop_weighted-quantile ()
  (FORALL
   (tuple l degree)
   (tuple (weighted-int-list) (degree))
   (let* ((weight-sum (weight-sum l))
          (q (quantiles:weighted-quantile degree l weight-sum))
          (left-part (lists:takewhile (match-lambda (((tuple v _)) (=< v q))) l))
          (right-part (lists:dropwhile (match-lambda (((tuple v _)) (< v q))) l)))
     (andalso
      (>= (weight-sum left-part) (* weight-sum degree))
      (>= (weight-sum right-part) (* weight-sum (- 1 degree)))
     ))))
