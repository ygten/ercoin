%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_sig).

-export(
   [verify_detached/3]).

-spec verify_detached(Sig :: binary(), Msg :: binary(), Account :: binary()) -> boolean().
%% TODO: Allow an account to be not only a public key, but also a Merkle root hash of keys (1-of-n). This, combined with Ed25519’s feature of compact n-of-n signatures, will allow for accounts controlled by arbitrary key combinations in non-complex cases.
verify_detached(Signature, Msg, PK) ->
    case byte_size(Signature) of
        64 ->
            case libsodium_crypto_sign_ed25519:verify_detached(Signature, Msg, PK) of
                0 ->
                    true;
                -1 ->
                    false
            end;
        _ ->
            false
    end.
