%% Licensed under the Apache License, Version 2.0 (the “License”);
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an “AS IS” BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(ercoin_abci).
-behaviour(gen_statem).
-behaviour(abci_app).
-export(
   [start_link/0]).

-compile({parse_transform, dynarec}).

%% Some directives to ease Triq testing (see https://github.com/ninenines/erlang.mk/issues/699) and expose testing code to Dialyzer.
-ifndef(TEST).
-define(TEST, true).
-endif.
%% -compile(export_all).

%% gen_statem
-export(
   [callback_mode/0,
    code_change/4,
    gossiping/3,
    committing/3,
    init/1,
    terminate/3]).

%% abci_app
-export([handle_request/1]).

-export(
   [app_hash/1]).

-include_lib("abci_server/include/abci.hrl").

-ifdef(TEST).
-include_lib("triq/include/triq.hrl").
-include_lib("eunit/include/eunit.hrl").
-endif.

-include_lib("include/ercoin.hrl").

start_link() ->
    gen_statem:start_link({local, ?MODULE}, ?MODULE, [], []).

callback_mode() ->
    state_functions.

code_change(_OldVsn, OldState, OldData, _Extra) ->
    {ok, OldState, OldData}.

init(_Args) ->
    {ok, gossiping, ercoin_genesis:genesis()}.

terminate(_Reason, _State, _Data) ->
    ok.

gossiping({call, From}, #'RequestBeginBlock'{}, Data) ->
    {next_state, committing, Data, {reply, From, #'ResponseBeginBlock'{}}};
gossiping({call, From}, #'RequestInitChain'{}, _Data) ->
    {keep_state_and_data, {reply, From, #'ResponseInitChain'{}}};
gossiping({call, From}, Request, Data) ->
    Response = gossiping(Request, Data),
    {keep_state_and_data, {reply, From, Response}}.

committing({call, From}, #'RequestCommit'{}, Data) ->
    Response = #'ResponseCommit'{data=app_hash(Data)},
    {next_state, gossiping, Data, {reply, From, Response}};
committing({call, From}, #'RequestEndBlock'{}, Data=#data{epoch_length=EpochLength, height=Height, fee_deposit=FeeDeposit, fresh_txs=FreshTxs}) ->
    {Data1, Diffs} =
        %% Drawing of new validators.
        case Height rem EpochLength of
            0 ->
                NewValidators = new_validators(Data),
                {Data#data{validators=NewValidators}, []};
            _ ->
                {Data, []}
        end,
    NewHeight = Data1#data.height + 1,
    NewFreshTxs = lists:dropwhile(fun ({ValidSince, _}) -> ValidSince < NewHeight - EpochLength + 2 end, FreshTxs),
    FeeGrantee = draw_fee_grantee(Data1),
    NewFeeGrantee = FeeGrantee#account{balance=FeeGrantee#account.balance + FeeDeposit},
    Data2 =
        remove_old_and_unlock_accounts(
          put_account(NewFeeGrantee, Data1#data{height=NewHeight, fresh_txs=NewFreshTxs, fee_deposit=0})),
    {keep_state, Data2, {reply, From, #'ResponseEndBlock'{diffs=Diffs}}};
committing({call, From}, Request, Data) when is_record(Request, 'RequestDeliverTx')->
    {Response, NewData} = committing(Request, Data),
    {keep_state, NewData, {reply, From, Response}};
committing({call, _}, _, _) ->
    {keep_state_and_data, postpone}.

handle_request(Request) ->
    gen_statem:call(?MODULE, Request).

%% Internal functions.

-spec gossiping(abci_server:request(), data()) -> abci_server:response().
%% @doc A helper function used to return responses for gossiping requests.
gossiping(#'RequestQuery'{path=Path, data=QueryData}, #data{accounts=Accounts}) ->
    {Code, ResponseValue, Error} =
        case iolist_to_binary(Path) of
            <<"account">> ->
                case gb_merkle_trees:lookup(QueryData, Accounts) of
                    none ->
                        {'BadNonce', <<>>, "Account not found."};
                    AccountBin ->
                        {'OK', AccountBin, []}
                end;
            _ ->
                {'UnknownRequest', <<>>, []}
        end,
    #'ResponseQuery'{code=Code, value=ResponseValue, log=Error};
gossiping(#'RequestCheckTx'{tx=TxBin}, Data) ->
    case tx_deserialize(TxBin) of
        none ->
            #'ResponseCheckTx'{code='EncodingError'};
        Tx ->
            case tx_error_code(Tx, Data) of
                none ->
                    #'ResponseCheckTx'{code='OK'};
                ErrorCode->
                    #'ResponseCheckTx'{code=ErrorCode}
            end
    end;
gossiping(#'RequestInfo'{}, Data) ->
    #'ResponseInfo'{
       last_block_app_hash=app_hash(Data),
       last_block_height=height(Data)}.

-spec committing(abci_server:request(), data()) -> {abci_server:response(), data()}.
committing(#'RequestDeliverTx'{tx=TxBin}, Data) ->
    case tx_deserialize(TxBin) of
        none ->
            {#'ResponseDeliverTx'{code='EncodingError'}, Data};
        Tx ->
            case tx_error_code(Tx, Data) of
                none ->
                    Response = #'ResponseDeliverTx'{code='OK'},
                    NewData = apply_tx(Tx, Data),
                    {Response, NewData};
                ErrorCode ->
                    Response = #'ResponseDeliverTx'{code=ErrorCode},
                    {Response, Data}
            end
    end.

-spec get_account(pk(), data()) -> account() | none.
get_account(Address, #data{accounts=Accounts}) ->
    case gb_merkle_trees:lookup(Address, Accounts) of
        none ->
            none;
        AccountBin ->
            account_deserialize({Address, AccountBin})
    end.

-spec put_account(account(), data()) -> data().
put_account(Account, Data=#data{accounts=Accounts}) ->
    {Address, AccountBin} = account_serialize(Account),
    NewAccounts = gb_merkle_trees:enter(Address, AccountBin, Accounts),
    Data#data{accounts=NewAccounts}.

-spec is_locked(account()) -> boolean().
is_locked(#account{locked_until=LockedUntil}) ->
    LockedUntil =/= none.

-spec put_vote(pk(), vote(), data()) -> data().
put_vote(Address, Vote, Data=#data{validators=Validators}) ->
    Power = get_voting_power(Address, Data),
    VoteBin = vote_serialize(Vote),
    NewValidators = gb_merkle_trees:enter(Address, <<Power, VoteBin/binary>>, Validators),
    Data#data{validators=NewValidators}.

-spec apply_tx(tx(), data()) -> data().
apply_tx(Tx, Data=#data{fee_deposit=FeeDeposit}) ->
    From = get_account(from(Tx), Data),
    Fee = fee(Tx, Data),
    NewFeeDeposit = FeeDeposit + Fee,
    NewFrom = From#account{balance=From#account.balance - Fee},
    apply_tx_1(Tx, put_account(NewFrom, Data#data{fee_deposit=NewFeeDeposit})).

-spec apply_tx_1(tx(), data()) -> data().
apply_tx_1(Tx=#transfer_tx{to=To, from=From, value=Value}, Data=#data{fresh_txs=FreshTxs, transfer_txs_hash=TransferTxsHash}) ->
    TxBin = tx_serialize(Tx),
    NewFreshTxs = ?SETS:add_element({get_valid_since(Tx), ?HASH(TxBin)}, FreshTxs),
    NewTransferTxsHash = ?HASH(<<TransferTxsHash/binary, TxBin/binary>>),
    FromAccount = get_account(From, Data),
    NewFromAccount = FromAccount#account{balance=FromAccount#account.balance - Value},
    Data1 = put_account(NewFromAccount, Data#data{fresh_txs=NewFreshTxs, transfer_txs_hash=NewTransferTxsHash}),
    ToAccount = get_account(To, Data1),
    NewToAccount = ToAccount#account{balance=ToAccount#account.balance + Value},
    put_account(NewToAccount, Data1);
apply_tx_1(#account_tx{valid_until=ValidUntil, to=ToAddress}, Data) ->
    NewAccount =
        case get_account(ToAddress, Data) of
            Account=#account{} ->
                Account#account{valid_until=ValidUntil};
            none ->
                #account{
                   valid_until=ValidUntil,
                   address=ToAddress}
        end,
    put_account(NewAccount, Data);
apply_tx_1(#lock_tx{locked_until=LockedUntil, address=Address}, Data) ->
    Account = get_account(Address, Data),
    put_account(Account#account{locked_until=LockedUntil}, Data);
apply_tx_1(Tx=#vote_tx{vote=Vote, address=Address}, Data=#data{fresh_txs=FreshTxs, vote_txs_hash=TxsHash}) ->
    TxBin = tx_serialize(Tx),
    NewTxsHash = ?HASH(<<TxsHash/binary, TxBin/binary>>),
    NewFreshTxs = ?SETS:add_element({get_valid_since(Tx), ?HASH(TxBin)}, FreshTxs),
    put_vote(Address, Vote, Data#data{fresh_txs=NewFreshTxs, vote_txs_hash=NewTxsHash}).

-spec tx_error_code(tx(), data()) -> none | 'BadNonce' | 'EncodingError' | 'InsufficientFunds'.
tx_error_code(Tx, Data=#data{height=Height, epoch_length=EpochLength}) ->
    ValidSince = get_valid_since(Tx),
    case ValidSince =:= none orelse ValidSince =< Height andalso ValidSince > Height - EpochLength of
        true ->
            tx_error_code_1(Tx, Data);
        _ ->
            'BadNonce'
    end.

-spec tx_error_code_1(tx(), data()) -> none | 'BadNonce' | 'EncodingError' | 'InsufficientFunds'.
tx_error_code_1(Tx=#transfer_tx{from=From, to=To, value=Value}, Data) ->
    %% We have little choice regarding error codes, so sometimes they are not adequate.
    case ?SETS:is_element({get_valid_since(Tx), ?HASH(tx_serialize(Tx))}, Data#data.fresh_txs) of
        false ->
            case get_account(To, Data) of
                none ->
                    'EncodingError';
                #account{} ->
                    case get_account(From, Data) of
                        none ->
                            'InsufficientFunds';
                        #account{balance=Balance, locked_until=LockedUntil} ->
                            case LockedUntil of
                                none ->
                                    case Balance < fee(Tx, Data) + Value of
                                        false ->
                                            none;
                                        true ->
                                            'InsufficientFunds'
                                    end;
                                _ ->
                                    'EncodingError'
                            end
                    end
            end;
        true ->
            'BadNonce'
    end;
tx_error_code_1(#account_tx{valid_until=ValidUntil, to=To, from=From}, Data) ->
    case From =:= To orelse not is_locked(get_account(From, Data)) of
        true ->
            case get_account(To, Data) of
                none ->
                    none;
                #account{valid_until=CurrentValidUntil} ->
                    case ValidUntil > CurrentValidUntil of
                        true ->
                            none;
                        false ->
                            'BadNonce'
                    end
            end;
        false ->
            'BadNonce'
    end;
tx_error_code_1(#lock_tx{address=Address, locked_until=LockedUntil}, Data) ->
    #account{locked_until=CurrentLockedUntil} = get_account(Address, Data),
    case CurrentLockedUntil =:= none orelse LockedUntil > CurrentLockedUntil of
        true ->
            none;
        false ->
            'BadNonce'
    end;
tx_error_code_1(Tx=#vote_tx{}, #data{fresh_txs=FreshTxs}) ->
    case ?SETS:is_element({get_valid_since(Tx), ?HASH(tx_serialize(Tx))}, FreshTxs) of
        false ->
            none;
        _ ->
            'BadNonce'
    end.

-spec account_serialize(account()) -> {Key :: binary(), Value :: binary()}.
account_serialize(
  #account{
     address=Address,
     balance=Balance,
     valid_until=ValidUntil,
     locked_until=LockedUntil}) ->
    {Address,
     case LockedUntil of
         none ->
             <<ValidUntil:4/unit:8, Balance:8/unit:8>>;
         _ ->
             <<ValidUntil:4/unit:8, Balance:8/unit:8, LockedUntil:4/unit:8>>
     end}.

-spec account_deserialize({Key :: binary(), Value :: binary()}) -> account().
account_deserialize({Address, AccountBin}) ->
    <<ValidUntil:4/unit:8, Balance:8/unit:8, Rest/binary>> = AccountBin,
    BaseAccount =
        #account{
           address=Address,
           balance=Balance,
           valid_until=ValidUntil},
    case Rest of
        <<>> ->
            BaseAccount;
        <<LockedUntil:4/unit:8>> ->
            BaseAccount#account{
              locked_until=LockedUntil}
    end.

-spec tx_deserialize(binary()) -> tx() | none.
tx_deserialize(TxBin) ->
    Tx =
        case TxBin of
            <<0, ValidSince:4/unit:8, From:32/binary, To:32/binary, Value:8/unit:8, MsgLength/integer, Msg:MsgLength/binary, Signature/binary>> ->
                #transfer_tx{
                   valid_since=ValidSince,
                   from=From,
                   to=To,
                   value=Value,
                   message=Msg,
                   signature=Signature};
            <<1, ValidUntil:4/unit:8, From:32/binary, To:32/binary, Signature/binary>> ->
                #account_tx{
                   valid_until=ValidUntil,
                   from=From,
                   to=To,
                   signature=Signature};
            <<2, LockedUntil:4/unit:8, Address:32/binary, Signature/binary>> ->
                #lock_tx{
                   locked_until=LockedUntil,
                   address=Address,
                   signature=Signature};
            <<3, ValidSince:4/unit:8, Address:32/binary, FeePerTx:8/unit:8, FeePer256B:8/unit:8, FeePerAccountDay:8/unit:8, Protocol:1/unit:8, Signature/binary>> ->
                #vote_tx{
                   address=Address,
                   vote=
                       #vote{
                          valid_since=ValidSince,
                          choices=#{
                            fee_per_tx => FeePerTx,
                            fee_per_256_bytes => FeePer256B,
                            fee_per_account_day => FeePerAccountDay,
                            protocol => Protocol}},
                   signature=Signature};
            _ ->
                none
        end,
    case Tx of
        none ->
            none;
        _ ->
            Signature1 = get_value(signature, Tx),
                SignedMsg = binary:part(TxBin, 0, byte_size(TxBin) - byte_size(Signature1)),
                case ercoin_sig:verify_detached(Signature1, SignedMsg, from(Tx))  of
                    true ->
                        Tx;
                    false ->
                        none
                end
    end.

-spec height(data()) -> block_height() | 0.
height(#data{height=Height}) ->
    Height.

-spec app_hash(data()) -> binary().
app_hash(
  #data{
     protocol=Protocol,
     accounts=Accounts,
     validators=Validators,
     transfer_txs_hash=TransferTxsHash,
     vote_txs_hash=VoteTxsHash,
     height=Height}) ->
    ValidatorsHash = gb_merkle_trees:root_hash(Validators),
    AccountsHash = gb_merkle_trees:root_hash(Accounts),
    OtherDataHash = ?HASH(<<Protocol, Height:4/unit:8, ValidatorsHash/binary, TransferTxsHash/binary, VoteTxsHash/binary>>),
    ?HASH(<<AccountsHash/binary, OtherDataHash/binary>>).

-spec fees(data()) -> map().
fees(Data) ->
    maps:remove(
      protocol,
      maps:merge(
        #{fee_per_256_bytes => 0,
          fee_per_account_day => 1,
          fee_per_tx => 0},
        voted_choices(Data))).

-spec div_ceil(integer(), integer()) -> integer().
%% @doc Divide integers with rounding up.
div_ceil(Dividend, Divisor) ->
    Dividend div Divisor + min(Dividend rem Divisor, 1).

-spec fee(tx(), data()) -> fee().
fee(#vote_tx{}, _Data) ->
    0;
fee(Tx=#transfer_tx{}, Data) ->
    #{fee_per_256_bytes := FPer256B,
      fee_per_tx := FPerTx}
        = fees(Data),
    byte_size(tx_serialize(Tx)) * FPer256B div 256 + FPerTx;
fee(Tx=#account_tx{valid_until=ValidUntil, to=Address}, Data=#data{height=Height}) ->
    ExtensionLength =
        case get_account(Address, Data) of
            none ->
                ValidUntil - Height;
            #account{valid_until=OldValidUntil} ->
                ValidUntil - OldValidUntil
        end,
    #{fee_per_256_bytes := FPer256B,
      fee_per_tx := FPerTx,
      fee_per_account_day := FPerAccountDay}
        = fees(Data),
    div_ceil(ExtensionLength * FPerAccountDay, 3600 * 24) + div_ceil(byte_size(tx_serialize(Tx)) * FPer256B, 256) + FPerTx;
fee(Tx=#lock_tx{locked_until=LockedUntil, address=Address}, Data=#data{height=Height}) ->
    #account{locked_until=OldLockedUntil} = get_account(Address, Data),
    ExtensionLength =
        case OldLockedUntil of
            none ->
                LockedUntil - Height;
            _ ->
                LockedUntil - OldLockedUntil
        end,
    #{fee_per_256_bytes := FPer256B,
      fee_per_tx := FPerTx,
      fee_per_account_day := FPerAccountDay}
        = fees(Data),
    div_ceil(ExtensionLength * FPerAccountDay, 3600 * 24) + div_ceil(byte_size(tx_serialize(Tx)) * FPer256B, 256) + FPerTx.

-spec from(tx()) -> pk().
from(#transfer_tx{from=From}) ->
    From;
from(#account_tx{from=From}) ->
    From;
from(#lock_tx{address=Address}) ->
    Address;
from(#vote_tx{address=Address}) ->
    Address.

-spec tx_serialize(tx()) -> binary().
tx_serialize(
  #transfer_tx{
     valid_since=ValidSince,
     from=From,
     to=To,
     value=Value,
     message=Message,
     signature=Signature}) ->
    MessageLength = byte_size(Message),
    <<0, ValidSince:4/unit:8, From/binary, To/binary, Value:8/unit:8, MessageLength, Message/binary, Signature/binary>>;
tx_serialize(
  #account_tx{
     valid_until=ValidUntil,
     from=From,
     to=To,
     signature=Signature}) ->
    <<1, ValidUntil:4/unit:8, From/binary, To/binary, Signature/binary>>;
tx_serialize(
 #lock_tx{
    locked_until=LockedUntil,
    address=Address,
    signature=Signature}) ->
    <<2, LockedUntil:4/unit:8, Address/binary, Signature/binary>>;
tx_serialize(
 #vote_tx{
    vote=
        #vote{
           valid_since=ValidSince,
           choices=Choices},
    address=Address,
    signature=Signature}) ->
    ChoicesBin = choices_serialize(Choices),
    <<3, ValidSince:4/unit:8, Address/binary, ChoicesBin/binary, Signature/binary>>.

-spec choices_serialize(choices()) -> binary().
choices_serialize(
  #{fee_per_tx := FeePerTx,
    fee_per_256_bytes := FeePer256B,
    fee_per_account_day := FeePerAccountDay,
    protocol := Protocol}) ->
    <<FeePerTx:8/unit:8, FeePer256B:8/unit:8, FeePerAccountDay:8/unit:8, Protocol>>.

-spec vote_serialize(vote() | none) -> binary().
vote_serialize(
  #vote{
     valid_since=ValidSince,
     choices=
         #{fee_per_tx := FeePerTx,
           fee_per_256_bytes := FeePer256B,
           fee_per_account_day := FeePerAccountDay,
           protocol := Protocol}}) ->
    <<ValidSince:4/unit:8, FeePerTx:8/unit:8, FeePer256B:8/unit:8, FeePerAccountDay:8/unit:8, Protocol>>;
vote_serialize(none) ->
    <<>>.

-spec vote_deserialize(binary()) -> vote() | none.
vote_deserialize(<<ValidSince:4/unit:8, FeePerTx:8/unit:8, FeePer256B:8/unit:8, FeePerAccountDay:8/unit:8, Protocol:1/unit:8>>) ->
    #vote{
       valid_since=ValidSince,
       choices=
           #{fee_per_tx => FeePerTx,
             fee_per_256_bytes => FeePer256B,
             fee_per_account_day => FeePerAccountDay,
             protocol => Protocol}};
vote_deserialize(<<>>) ->
    none.

-spec get_valid_since(tx()) -> block_height() | none.
get_valid_since(#transfer_tx{valid_since=ValidSince}) ->
    ValidSince;
get_valid_since(#vote_tx{vote=#vote{valid_since=ValidSince}}) ->
    ValidSince;
get_valid_since(_) ->
    none.

-spec voting_resolution(data()) -> pos_integer().
voting_resolution(_) ->
    64.

-spec entropy(data()) -> binary().
%% TODO: Use a good source of entropy instead of block hash. Maybe the NIST beacon initially and a verifiable source later.
%% “Version 2.0 of the NIST beacon is planned to be deployed in the Summer of 2017. This version will make significant changes to the REST interface.”
entropy(#data{last_block_hash=Hash}) ->
    Hash.

-spec new_validators(data()) -> gb_merkle_trees:tree().
new_validators(Data=#data{accounts=Accounts, height=Height}) ->
    Resolution = voting_resolution(Data),
    {ScoresAddresss, _ScoreSum} =
        gb_merkle_trees:foldr(
          fun (AccountSerialized, {ScoresAddresss1, ScoreSum1}) ->
                  #account{address=Address, locked_until=LockedUntil, balance=Balance} = account_deserialize(AccountSerialized),
                  case LockedUntil of
                      none ->
                          {ScoresAddresss1, ScoreSum1};
                      _ ->
                          Handicap = (LockedUntil - Height) * Balance,
                          Entropy = entropy(Data),
                          RandomInt = binary:decode_unsigned(binary:part(?HASH(<<0, Entropy/binary, Address/binary>>), 0, 8)),
                          Score = RandomInt * Handicap,
                          {ordsets:add_element({Score, Address}, ScoresAddresss1), ScoreSum1 + Score}
                  end
          end,
          {[], 0},
          Accounts),
    %% TODO: Pick an appropriate apportionment method, maybe Sainte-Laguë or Hare-Niemeyer.
    [{_, Address}|_] = ScoresAddresss,
    gb_merkle_trees:from_list([{Address, binary:encode_unsigned(Resolution)}]).

-spec draw_fee_grantee(data()) -> account().
draw_fee_grantee(Data) ->
    draw_existing_validator(Data, 2).

-spec draw_existing_validator(data(), non_neg_integer()) -> account().
draw_existing_validator(
  Data=#data{validators=Validators, last_block_hash=LastBlockHash},
  Modifier) ->
    {DrawnValidator, _} =
        gb_merkle_trees:foldr(
          fun ({Address, <<VotePower/integer, _/binary>>}, {CurrentBest, BestScore}) ->
                  NewScore = VotePower * binary:decode_unsigned(?HASH(<<Address/binary, LastBlockHash/binary, Modifier>>)),
                  case NewScore > BestScore of
                      true ->
                          {get_account(Address, Data), NewScore};
                      false ->
                          {CurrentBest, BestScore}
                  end
          end,
          {none, 0},
          Validators),
    DrawnValidator.

-spec get_voting_power(pk(), data()) -> voting_power().
get_voting_power(Address, Data) ->
    case gb_merkle_trees:lookup(Address, Data#data.validators) of
        none ->
            0;
        PowerBin ->
            binary:decode_unsigned(PowerBin)
    end.

-spec remove_old_and_unlock_accounts(data()) -> data().
remove_old_and_unlock_accounts(Data=#data{accounts=AccountsTree, height=Height}) ->
    NewAccountsTree = remove_old_and_unlock_accounts_from_tree(AccountsTree, Height),
    Data#data{accounts=NewAccountsTree}.

-spec remove_old_and_unlock_accounts_from_tree(gb_merkle_trees:tree(), block_height()) -> gb_merkle_trees:tree().
remove_old_and_unlock_accounts_from_tree(AccountsTree, Height) ->
    gb_merkle_trees:foldr(
      fun ({Address, AccountBin}, TreeAcc) ->
              case account_deserialize({Address, AccountBin}) of
                  #account{valid_until=ValidUntil} when ValidUntil < Height ->
                      gb_merkle_trees:delete(Address, TreeAcc);
                  Account=#account{locked_until=LockedUntil} when LockedUntil < Height ->
                      {_, NewAccountBin} = account_serialize(Account#account{locked_until=none}),
                      gb_merkle_trees:enter(Address, NewAccountBin, TreeAcc);
                  #account{} ->
                      TreeAcc
              end
      end,
      AccountsTree,
      AccountsTree).

-spec merge_choices_vps(choices(), voting_power(), map()) -> map().
merge_choices_vps(SingleChoices, VP, ChoicesVPs) ->
    Keys = maps:keys(ChoicesVPs),
    lists:foldl(
      fun (Key, Acc) ->
              ChoiceVPsForKey = maps:get(Key, Acc),
              Acc#{Key => [{maps:get(Key, SingleChoices), VP}|ChoiceVPsForKey]}
      end,
      ChoicesVPs,
      Keys).

-spec voted_choices(data()) -> choices().
voted_choices(#data{validators=Validators}) ->
    {VoteSum, ChoicesVPs} =
        gb_merkle_trees:foldr(
          fun ({_, <<VP/integer, VoteBin/binary>>}, {VPSum, ChoicesVPs}) ->
                  case vote_deserialize(VoteBin) of
                      none ->
                          {VPSum, ChoicesVPs};
                      #vote{choices=Choices} ->
                          {VPSum + VP, merge_choices_vps(Choices, VP, ChoicesVPs)}
                  end
          end,
          {0, #{}},
          Validators),
    SortedChoicesVPs = maps:map(fun (_, ChoiceVPs) -> lists:sort(ChoiceVPs) end, ChoicesVPs),
    maps:map(
      fun (_, ChoiceVPs) -> quantiles:'weighted-quantile'(0.5, ChoiceVPs, VoteSum) end,
      SortedChoicesVPs).

%% TODO: Try to put tests into a separate module.
-ifdef(TEST).
-type sk() :: <<_:512>>.
-type sks() :: {AddressTree :: gb_merkle_trees:tree(), SKTree :: gb_merkle_trees:tree()}.

-spec delete_account(pk(), data()) -> data().
delete_account(Address, Data=#data{accounts=Accounts}) ->
    NewAccounts = gb_merkle_trees:delete(Address, Accounts),
    Data#data{accounts=NewAccounts}.

lookup_balance(Address, #data{accounts=AccountsTree}) ->
    #account{balance=Balance} = account_deserialize({Address, gb_merkle_trees:lookup(Address, AccountsTree)}),
    Balance.

set_valid_since(ValidSince, Tx, SK) ->
    sign_tx(set_valid_since_1(ValidSince, Tx), SK).

set_valid_since_1(ValidSince, Tx=#transfer_tx{}) ->
    Tx#transfer_tx{valid_since=ValidSince};
set_valid_since_1(ValidSince, Tx=#vote_tx{}) ->
    Tx#vote_tx{vote=Tx#vote_tx.vote#vote{valid_since=ValidSince}}.

-spec sign_tx(tx(), sk() | map() | sks()) -> tx().
sign_tx(Tx, SKs) when is_map(SKs) ->
    SK = maps:get(from(Tx), SKs),
    sign_tx(Tx, SK);
sign_tx(Tx, SK) ->
    ToSign = to_sign(Tx),
    Signature = sign_detached(ToSign, SK),
    set_value(signature, Signature, Tx).

-spec sign_detached(binary(), sk()) -> binary().
sign_detached(Msg, SK) ->
    libsodium_crypto_sign_ed25519:detached(Msg, SK).

-spec sign(binary(), sk()) -> binary().
sign(Msg, SK) ->
    Signature = sign_detached(Msg, SK),
    <<Msg/binary, Signature/binary>>.

-spec get_vote(pk(), data()) -> vote() | none.
get_vote(Address, #data{validators=Validators}) ->
    <<_:1/unit:8, VoteBin/binary>> = gb_merkle_trees:lookup(Address, Validators),
    vote_deserialize(VoteBin).

-spec to_sign(tx()) -> binary().
to_sign(
  #transfer_tx{
        valid_since=ValidSince,
        from=From,
        to=To,
        value=Value,
        message=Message}) ->
    MessageLength = byte_size(Message),
    <<0, ValidSince:4/unit:8, From/binary, To/binary, Value:8/unit:8, MessageLength, Message/binary>>;
to_sign(#account_tx{valid_until=ValidUntil, from=From, to=To}) ->
    <<1, ValidUntil:4/unit:8, From/binary, To/binary>>;
to_sign(#lock_tx{address=Address, locked_until=LockedUntil}) ->
    <<2, LockedUntil:4/unit:8, Address/binary>>;
to_sign(
  #vote_tx{
     address=Address,
     vote=
         #vote{
            valid_since=ValidSince,
            choices=Choices}}) ->
    ChoicesBin = choices_serialize(Choices),
    <<3, ValidSince:4/unit:8, Address/binary, ChoicesBin/binary>>.

money_supply(#data{accounts=Accounts, fee_deposit=FeeDeposit}) ->
    AccountsBalance =
        gb_merkle_trees:foldr(
          fun (AccountSerialized, Sum) ->
                  #account{balance=Balance} = account_deserialize(AccountSerialized),
                  Balance + Sum
          end,
          0,
          Accounts),
    AccountsBalance + FeeDeposit.

-spec end_block(data()) -> data().
end_block(Data) ->
    {keep_state, NewData, {reply, "from", #'ResponseEndBlock'{}}} =
        committing({call, "from"}, #'RequestEndBlock'{}, Data),
    NewData.

-define(
   SAMPLE_TRANSFER_TX,
   #transfer_tx{
      valid_since=1234,
      %% Secret key: <<241,233,155,25,231,68,253,237,185,176,117,196,7,138,248,241,14,148,35,27,241,3,132,194,142,70,50,81,135,104,72,21,236,168,42,39,132,221,157,252,126,127,245,49,190,10,84,207,79,253,85,117,4,82,163,98,146,75,91,17,19,215,36,37>>.
      from= <<236,168,42,39,132,221,157,252,126,127,245,49,190,10,84,207,79,253,85,117,4,82,163,98,146,75,91,17,19,215,36,37>>,
      to= <<0:32/unit:8>>,
      value=123456,
      message= <<"Zażółć gęślą jaźń."/utf8>>,
      signature= <<175,71,238,101,235,169,214,87,200,229,114,139,31,18,224,241,86,128,170,117,162,14,132,221,46,227,217,31,3,137,28,61,126,167,204,237,192,14,61,12,32,149,101,240,124,231,12,109,165,239,19,37,232,202,119,34,86,216,93,69,18,253,68,5>>}).
-define(
   SAMPLE_TRANSFER_TX_SERIALIZED,
   <<0,
     0,0,4,210,
     236,168,42,39,132,221,157,252,126,127,245,49,190,10,84,207,79,253,85,117,4,82,163,98,146,75,91,17,19,215,36,37,
     0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
     0,0,0,0,0,1,226,64,
     27,
     90,97,197,188,195,179,197,130,196,135,32,103,196,153,197,155,108,196,133,32,106,97,197,186,197,132,46,
     175,71,238,101,235,169,214,87,200,229,114,139,31,18,224,241,86,128,170,117,162,14,132,221,46,227,217,31,3,137,28,61,126,167,204,237,192,14,61,12,32,149,101,240,124,231,12,109,165,239,19,37,232,202,119,34,86,216,93,69,18,253,68,5>>).

tx_serialization_test_() ->
    [?_assertEqual(?SAMPLE_TRANSFER_TX_SERIALIZED, tx_serialize(?SAMPLE_TRANSFER_TX)),
     ?_assertEqual(?SAMPLE_TRANSFER_TX, tx_deserialize(?SAMPLE_TRANSFER_TX_SERIALIZED))].

%% Triq generators.

pk_sk() ->
    domain(
      pk_sk,
      fun (Self, _) -> {Self, libsodium_crypto_sign_ed25519:keypair()} end,
      fun (Self, Value) -> {Self, Value} end).

pk_sks() ->
    oneof(
    [fun pk_sk/0]).
     %% fun merkle_pk_sks/0]).

%% merkle_pk_sks() ->
%%     ?LET(
%%        AddresssSKs,
%%        non_empty(list(pk_sk())),
%%        begin
%%            %% TODO: gb_merkle_trees is not going to create all shapes of trees, so do it in another way.
%%            AddressTree = gb_merkle_trees:from_list([{Address, <<>>} || {Address, _} <- AddresssSKs]),
%%            SKTree = gb_merkle_trees:from_list(AddresssSKs),
%%            %% The second element of the tuple is not really a plain secret key nor a dictionary. AddressTree is provided for convenience, to easily obtain Merkle proofs.
%%            {gb_merkle_trees:root_hash(AddressTree), {AddressTree, SKTree}}
%%        end).

partition(Int) ->
    domain(
      partition,
      fun (Self, _) -> {Self, partition_1(Int, [])} end,
      fun (Self, Partition) ->
              case Partition of
                  [_] ->
                      {Self, Partition};
                  _ ->
                      Len = length(Partition),
                      Idx1 = triq_rnd:uniform(Len),
                      Idx2 = triq_rnd:uniform(Len - 1),
                      Val1 = lists:nth(Idx1, Partition),
                      Val2 = lists:nth(Idx2, lists:delete(Val1, Partition)),
                      JoinedVal = Val1 + Val2,
                      NewPartition = [JoinedVal|lists:delete(Val2, lists:delete(Val1, Partition))],
                      {Self, NewPartition}
              end
      end).

partition_1(0, Acc) ->
    Acc;
partition_1(Int, Acc) ->
    Val = triq_rnd:uniform(Int),
    partition_1(Int - Val, [Val|Acc]).

pos_int_lte_with_partition(UpperBound) ->
    ?LET(
       I,
       choose(1, UpperBound),
       {I, partition(I)}).

partition_lte(UpperBound) ->
    ?LET(
       {_, Partition},
       pos_int_lte_with_partition(UpperBound),
       Partition).

hash() ->
    binary(32).

block_height() ->
    choose(1, ?MAX_BLOCK_HEIGHT div 2).

epoch_length() ->
    ?LET(
       I,
       pos_integer(),
       59 + I).

fee() ->
    non_neg_integer().

balance() ->
    non_neg_integer().

message() ->
    ?SUCHTHAT(
       Bin,
       binary(),
       byte_size(Bin) < 256).

vote(#data{height=Height, epoch_length=EpochLength}) ->
    vote(Height, EpochLength).

vote(Height, EpochLength) ->
    ?LET(
       {ValidSince, Choices},
       {valid_since(Height, EpochLength), choices()},
       #vote{
          valid_since=ValidSince,
          choices=Choices}).

vote_tx_sk() ->
    ?LET(
       {{Address, SK}, ValidSince, Choices},
       {pk_sks(), block_height(), choices()},
       {sign_tx(
          #vote_tx{
             address=Address,
             vote=
                 #vote{
                    valid_since=ValidSince,
                    choices=Choices},
             signature= <<0:512>>},
          SK),
        SK}).

vote_tx() ->
    ?LET(
       {Tx, _},
       vote_tx_sk(),
       Tx).

vote_tx(Data, Address, SK) ->
    ?LET(
       Vote,
       vote(Data),
       sign_tx(
         #vote_tx{
            address=Address,
            vote=Vote,
            signature= <<0:512>>},
         SK)).

choices() ->
    ?LET(
       [FeePerTx, FeePer256B, FeePerAccountDay],
       vector(3, fee()),
       #{fee_per_tx => FeePerTx,
         fee_per_256_bytes => FeePer256B,
         fee_per_account_day => FeePerAccountDay,
         protocol => 1}).

account_sk(Locked, BlockHeight) ->
    LockedForGen =
        case Locked of
            none ->
                oneof([0, pos_integer(), none]);
            true ->
                oneof([0, pos_integer()])
        end,
    ?LET(
       {{Address, SK}, Balance, ValidFor, LockedFor},
       {pk_sks(), balance(), oneof([0, pos_integer()]), LockedForGen},
       begin
           LockedUntil =
               case LockedFor of
                   none ->
                       none;
                   _ ->
                       BlockHeight + LockedFor
               end,
           {#account{
               address=Address,
               balance=Balance,
               valid_until=BlockHeight + ValidFor,
               locked_until=LockedUntil},
            SK}
       end).

account_sk(Locked) ->
    account_sk(Locked, 1).

account_sk() ->
    account_sk(none).

account() ->
    ?LET(
       {Account, _},
       account_sk(),
       Account).

-spec accounts_tree_from_list(list(account())) -> gb_merkle_trees:tree().
accounts_tree_from_list(Accounts) ->
    accounts_tree_from_list(Accounts, gb_merkle_trees:empty()).

accounts_tree_from_list([], Acc) ->
    Acc;
accounts_tree_from_list([Account|Tail], Acc) ->
    {Key, Value} = account_serialize(Account),
    NewAcc = gb_merkle_trees:enter(Key, Value, Acc),
    accounts_tree_from_list(Tail, NewAcc).

validators_tree_from_list([], Acc) ->
    Acc;
validators_tree_from_list([{{#account{address=Address}, Vote}, VotePower}|Tail], Acc) ->
    VoteSerialized = vote_serialize(Vote),
    VotePowerBin = binary:encode_unsigned(VotePower),
    NewAcc = gb_merkle_trees:enter(Address, <<VotePowerBin/binary, VoteSerialized/binary>>, Acc),
    validators_tree_from_list(Tail, NewAcc).

-spec validators_tree_from_list(list({{account(), vote() | none}, pos_integer()})) -> gb_merkle_trees:tree().
validators_tree_from_list(AccountsVotesVotePowers) ->
    validators_tree_from_list(AccountsVotesVotePowers, gb_merkle_trees:empty()).

data_sks(MustHaveAnUnlockedAccount) ->
    ?LET(
       {Height,
        TransferTxsHash,
        VoteTxsHash,
        LastBlockHash,
        FeeDeposit,
        EpochLength,
        VotesPartition,
        FreshTxsStub},
       {block_height(),
        hash(),
        hash(),
        hash(),
        fee(),
        epoch_length(),
        partition_lte(255),
        list({block_height(), hash()})},
       ?LET(
          {ValidatorsSKsVotes,
           AccountsSKs},
          {vector(length(VotesPartition), {account_sk(true, Height), oneof([vote(Height, EpochLength), none])}),
           case MustHaveAnUnlockedAccount of
               true ->
                   ?SUCHTHAT(
                      AccountsSKs,
                      list(account_sk(none, Height)),
                      lists:any(
                        fun ({Account, _}) -> not is_locked(Account) end,
                        AccountsSKs));
               false ->
                   list(account_sk(none, Height))
           end},
          begin
              FreshTxs =
                  [{max(1, Height - (TxHeight rem EpochLength)), Hash} || {TxHeight, Hash} <- FreshTxsStub],
              Validators = [Validator || {{Validator, _}, _} <- ValidatorsSKsVotes],
              ValidatorsVotes = [{Validator, Vote} || {{Validator, _}, Vote} <- ValidatorsSKsVotes],
              ValidatorsVotesVPs = lists:zip(ValidatorsVotes, VotesPartition),
              ValidatorsSKs = [{Validator, SK} || {{Validator, SK}, _} <- ValidatorsSKsVotes],
              Accounts =
                  [Account || {Account, _} <- AccountsSKs],
              SKs =
                  maps:from_list(
                    [{Address, SK} || {#account{address=Address}, SK} <- ValidatorsSKs ++ AccountsSKs]),
              AllAccounts = Accounts ++ Validators,
              {#data{
                  height=Height,
                  epoch_length=EpochLength,
                  fee_deposit=FeeDeposit,
                  transfer_txs_hash=TransferTxsHash,
                  vote_txs_hash=VoteTxsHash,
                  fresh_txs=?SETS:from_list(FreshTxs),
                  validators=validators_tree_from_list(ValidatorsVotesVPs),
                  accounts=accounts_tree_from_list(AllAccounts),
                  last_block_hash=LastBlockHash},
               SKs}
          end)).

data_sks() ->
    data_sks(false).

data() ->
    ?LET(
       {Data, _},
       data_sks(),
       Data).

-spec filter_accounts(fun((account(), data()) -> boolean()), data()) -> list(account()).
filter_accounts(Pred, Data=#data{accounts=AccountsTree}) ->
    gb_merkle_trees:foldr(
      fun (AccountSerialized, Acc) ->
              Account = account_deserialize(AccountSerialized),
              case Pred(Account, Data) of
                  true ->
                      [Account|Acc];
                  false ->
                      Acc
              end
      end,
      [],
      AccountsTree).

account_from(Data, Type) ->
    AccountFilter =
       case Type of
           locked ->
               fun (#account{locked_until=LockedUntil}, _) -> LockedUntil =/= none end;
           validator ->
               fun (#account{address=Address}, Data1) -> get_voting_power(Address, Data1) > 0 end;
           any ->
               fun (_, _) -> true end;
           unlocked ->
               fun (#account{locked_until=LockedUntil}, _) -> LockedUntil =:= none end
       end,
    AccountsList = filter_accounts(AccountFilter, Data),
    elements(AccountsList).

account_from(Data) ->
    account_from(Data, any).

data_sks_and_account(Type) ->
    ?LET(
       {Data, SKs},
       data_sks(),
       ?LET(
          Account,
          account_from(Data, Type),
          {{Data, SKs}, Account})).

data_sks_and_account() ->
    data_sks_and_account(any).

data_with_account_sk_and_vote_tx() ->
    ?LET(
       {{Data, SKs}, Account},
       data_sks_and_account(validator),
       begin
           SK = maps:get(Account#account.address, SKs),
           {Data, {Account, SK}, vote_tx(Data, Account#account.address, SK)}
       end).

data_with_vote_tx() ->
    ?LET(
       {Data, _, VoteTx},
       data_with_account_sk_and_vote_tx(),
       {Data, VoteTx}).

data_with_account() ->
    ?LET(
       {{Data, _}, Account},
       data_sks_and_account(),
       {Data, Account}).

data_sks_and_lock_tx() ->
    ?LET(
       {{{Data=#data{height=Height}, SKs}, #account{address=Address, locked_until=LockedUntil}}, LockedFor},
       {data_sks_and_account(), pos_integer()},
       begin
           NewLockedUntil =
               case LockedUntil of
                   none ->
                       Height + LockedFor;
                   _ ->
                       LockedUntil + LockedFor
               end,
           Tx =
               sign_tx(
                 #lock_tx{
                    address=Address,
                    locked_until=NewLockedUntil,
                    signature= <<0:512>>},
                 SKs),
           {{make_sufficient_balance(Data, Tx), SKs}, Tx}
       end).

valid_since(#data{height=Height, epoch_length=EpochLength}) ->
    valid_since(Height, EpochLength).

valid_since(Height, EpochLength) ->
    choose(max(Height - EpochLength + 1, 1), Height).


-spec make_sufficient_balance(data(), tx()) -> data().
make_sufficient_balance(Data, Tx) ->
    Account = get_account(from(Tx), Data),
    Fee = fee(Tx, Data),
    MinBalance =
        case Tx of
            #transfer_tx{value=Value} ->
                Fee + Value;
            _ ->
                Fee
        end,
    NewAccount = Account#account{balance=max(MinBalance, Account#account.balance)},
    put_account(NewAccount, Data).

transfer_tx({Data, SKs}, From, To) ->
    ?LET(
       {Value, ValidSince, Message},
       {choose(0, From#account.balance), valid_since(Data), message()},
       sign_tx(
         #transfer_tx{
            valid_since=ValidSince,
            from=From#account.address,
            to=To#account.address,
            message=Message,
            value=Value,
            signature= <<0:512>>},
         SKs
        )).

account_tx({Data, SKs}, From, ToAddress) ->
    ?LET(
       ValidFor,
       pos_integer(),
       begin
           ValidUntil =
               case get_account(ToAddress, Data) of
                   none ->
                       Data#data.height + ValidFor;
                   #account{valid_until=ExistingValidUntil} ->
                       ExistingValidUntil + ValidFor
               end,
           sign_tx(
             #account_tx{
                from=From#account.address,
                to=ToAddress,
                valid_until=ValidUntil,
                signature= <<0:512>>},
             SKs)
       end).

data_sks_and_transfer_tx() ->
    ?LET(
       {Data, SKs},
       data_sks(true),
       ?LET(
          {From, To},
          {account_from(Data, unlocked), account_from(Data)},
          ?LET(
             Tx,
             transfer_tx({Data, SKs}, From, To),
             {{make_sufficient_balance(Data, Tx), SKs}, Tx}))).

data_sks_and_account_tx() ->
    ?LET(
       {{Data, SKs}, From=#account{address=FromAddress}},
       data_sks_and_account(),
       ?LET(
          {ToAddress, ToSK},
          case is_locked(From) of
              true ->
                  {FromAddress, maps:get(FromAddress, SKs)};
              false ->
                  oneof(
                    [?LET(
                        #account{address=ToAddress},
                        account_from(Data),
                        {ToAddress, maps:get(ToAddress, SKs)}),
                     pk_sk()])
          end,
          ?LET(
             Tx,
             account_tx({Data, SKs}, From, ToAddress),
             {{make_sufficient_balance(Data, Tx), maps:put(ToAddress, ToSK, SKs)}, Tx}))).

data_with_account_tx() ->
    ?LET(
       {{Data, _}, Tx},
       data_sks_and_account_tx(),
       {Data, Tx}).

data_with_transfer_tx() ->
    ?LET(
       {{Data, _}, Tx},
       data_sks_and_transfer_tx(),
       {Data, Tx}).

data_with_account_sk_and_transfer_tx() ->
    ?LET(
       {{Data, SKs}, Tx},
       data_sks_and_transfer_tx(),
       {Data, {get_account(Tx#transfer_tx.from, Data), maps:get(Tx#transfer_tx.from, SKs)}, Tx}).

data_with_lock_tx() ->
    ?LET(
       {{Data, _}, Tx},
       data_sks_and_lock_tx(),
       {Data, Tx}).

data_with_valid_tx() ->
    oneof(
      [fun data_with_transfer_tx/0,
       fun data_with_lock_tx/0]).

transfer_tx() ->
    ?LET(
       {_, ValidTx},
       data_with_transfer_tx(),
       ValidTx).

valid_lock_tx() ->
    ?LET(
       {_, Tx},
       data_with_lock_tx(),
       Tx).

account_tx() ->
    ?LET(
       {{_, _}, Tx},
       data_sks_and_account_tx(),
       Tx).

data_with_invalid_tx_bin() ->
    %% Tx is returned as binary.
    oneof(
      [%% Random binary.
       ?LET(
          {Data, RandomBin},
          {data(), binary()},
          {Data, RandomBin}),
       %% Insufficient balance for transfer.
       ?LET(
          {{{Data, SKs}, Tx}, LackingFunds},
          {data_sks_and_transfer_tx(), pos_integer()},
          begin
              Account = get_account(from(Tx), Data),
              InvalidValue = max(0, Account#account.balance - fee(Tx, Data) + LackingFunds),
              InvalidTx =
                  sign_tx(Tx#transfer_tx{value=InvalidValue}, SKs),
              {Data, tx_serialize(InvalidTx)}
          end),
       %% Bad signature.
       ?LET(
          {{Data, ValidTx}, InvalidSig},
          {data_with_valid_tx(), oneof([binary(64), binary()])},
          begin
              ToSign = to_sign(ValidTx),
              {Data, <<ToSign/binary, InvalidSig/binary>>}
          end),
       %% Lock not longer than existing.
       ?LET(
          {{{Data, _}, Tx}, ShorterFor},
          {data_sks_and_lock_tx(), non_neg_integer()},
          begin
              Account = get_account(from(Tx), Data),
              {put_account(Account#account{locked_until=Tx#lock_tx.locked_until + ShorterFor}, Data), tx_serialize(Tx)}
          end),
       %% Validity not longer than existing.
       ?LET(
          {{{Data, _}, Tx}, ShorterFor},
          {?SUCHTHAT(
              {{Data, _}, #account_tx{to=ToAddress}},
              data_sks_and_account_tx(),
              get_account(ToAddress, Data) =/= none),
           non_neg_integer()},
          begin
              To = get_account(Tx#account_tx.to, Data),
              {put_account(To#account{valid_until=Tx#account_tx.valid_until + ShorterFor}, Data), tx_serialize(Tx)}
          end),
       %% From locked and To other than From.
       ?LET(
          {{{Data, _}, Tx}, LockedFor},
          {?SUCHTHAT(
              {_, #account_tx{from=From, to=To}},
              data_sks_and_account_tx(),
              From =/= To),
           pos_integer()},
          begin
              Account = get_account(from(Tx), Data),
              {put_account(Account#account{locked_until=Data#data.height + LockedFor}, Data), tx_serialize(Tx)}
          end),
       %% Destination not existing.
       ?LET(
          {Data, ValidTx},
          data_with_transfer_tx(),
          {delete_account(ValidTx#transfer_tx.to, Data), tx_serialize(ValidTx)}),
       %% We could check also destination that will expire in next block, but… that expiration cannot be certain, as account’s validity can be extended after a transaction is delivered.
       %%
       %% From not existing.
       ?LET(
          {Data, ValidTx},
          data_with_transfer_tx(),
          {delete_account(ValidTx#transfer_tx.from, Data), tx_serialize(ValidTx)}),
       %% From locked.
       ?LET(
          {Data, SKs},
          data_sks(),
          ?LET(
             {From, To},
             {account_from(Data, locked), account_from(Data)},
             ?LET(
                Tx,
                %% We abuse the fact that transfer_tx doesn’t check whether account is locked.
                transfer_tx({Data, SKs}, From, To),
                {Data, tx_serialize(Tx)}))),
       %% Tx not valid yet.
       ?LET(
          {Data, {_, SK}, Tx},
          oneof(
            [fun data_with_account_sk_and_transfer_tx/0,
             fun data_with_account_sk_and_vote_tx/0]),
          ?LET(
             ValidIn,
             pos_integer(),
             {Data, tx_serialize(set_valid_since(Data#data.height + ValidIn, Tx, SK))})),
       %% Tx outdated.
       ?LET(
          {Data, {_, SK}, Tx},
          ?SUCHTHAT(
             {#data{height=Height, epoch_length=EpochLength}, _, _},
             oneof(
               [fun data_with_account_sk_and_transfer_tx/0,
                fun data_with_account_sk_and_vote_tx/0]),
             Height > EpochLength),
          ?LET(
             ExpiredBy,
             pos_integer(),
             {Data, tx_serialize(set_valid_since(Data#data.height - Data#data.epoch_length - ExpiredBy + 1, Tx, SK))})),
       %% Incorrectly specified message length.
       ?LET(
          {{{Data, SKs}, ValidTx}, InvalidLength},
          ?SUCHTHAT(
             {{_, ValidTx}, InvalidLength},
             {data_sks_and_transfer_tx(), choose(0, 255)},
             InvalidLength =/= byte_size(ValidTx#transfer_tx.message)),
          begin
              Message = ValidTx#transfer_tx.message,
              ToSign = to_sign(ValidTx),
              TxHead = binary:part(ToSign, 0, byte_size(ToSign) - byte_size(Message) - 1),
              NewToSign = <<TxHead/binary, InvalidLength, Message/binary>>,
              {Data, sign(NewToSign, maps:get(from(ValidTx), SKs))}
          end),
       %% Replayed transfer tx or vote tx.
       ?LET(
          {Data, Tx},
          oneof(
            [fun data_with_transfer_tx/0,
             fun data_with_vote_tx/0]),
          begin
              TxBin = tx_serialize(Tx),
              #data{fresh_txs=FreshTxs} = Data,
              NewFreshTxs = ?SETS:add_element({get_valid_since(Tx), ?HASH(TxBin)}, FreshTxs),
              {Data#data{fresh_txs=NewFreshTxs}, TxBin}
          end)
       %% TODO: Valid tx with random bytes appended/prepended.
       %% TODO: Maybe ensure that no more than one vote tx per validator can be included in one block.
      ]).

data_with_tx() ->
    oneof(
      [fun data_with_transfer_tx/0,
       fun data_with_lock_tx/0,
       fun data_with_account_tx/0,
       fun data_with_vote_tx/0]).

tx() ->
    oneof(
      [fun transfer_tx/0,
       fun valid_lock_tx/0,
       fun account_tx/0,
       fun vote_tx/0]).

%% Properties.

prop_account_serialization() ->
    ?FORALL(A, account(), account_deserialize(account_serialize(A)) =:= A).

prop_tx_serialization() ->
    ?FORALL(Tx, tx(), tx_deserialize(tx_serialize(Tx)) =:= Tx).

prop_account_query_returns_serialized_account() ->
    ?FORALL(
       {Data, Account},
       data_with_account(),
       begin
           #'ResponseQuery'{value=ResponseValue} =
               gossiping(
                 #'RequestQuery'{
                    path="account",
                    data=Account#account.address},
                 Data),
           {_Address, Value} = account_serialize(Account),
           Value =:= ResponseValue
       end).

prop_account_query_returns_bad_nonce_for_non_existing_account() ->
    ?FORALL(
       {Data, NonExistingAddress},
       {data(), binary(32)},
       begin
           #'ResponseQuery'{code=ResponseCode} =
               gossiping(
                 #'RequestQuery'{
                    path="account",
                    data=NonExistingAddress},
                 Data),
           ResponseCode =/= 'OK'
       end).

prop_unknown_query_returns_unknown_request_code() ->
    ?FORALL(
       Data,
       data(),
       begin
           #'ResponseQuery'{code=ResponseCode} =
               gossiping(
                 #'RequestQuery'{
                    path="non-existent",
                    data= <<"foo">>},
                 Data),
           'UnknownRequest' =:= ResponseCode
       end).

prop_valid_tx_is_positively_checked() ->
    ?FORALL(
       {Data, ValidTx},
       data_with_valid_tx(),
       begin
           #'ResponseCheckTx'{code=ResponseCode} =
               gossiping(
                 #'RequestCheckTx'{
                    tx=tx_serialize(ValidTx)},
                Data),
           'OK' =:= ResponseCode
       end).

prop_transfer_tx_adds_balance_to_destination() ->
    ?FORALL(
       {Data, Tx=#transfer_tx{from=From, to=To}},
       data_with_transfer_tx(),
       ?IMPLIES(
          From =/= To,
          begin
              {#'ResponseDeliverTx'{code='OK'}, NewData} =
                  committing(
                    #'RequestDeliverTx'{
                       tx=tx_serialize(Tx)},
                    Data),
              To = Tx#transfer_tx.to,
              lookup_balance(To, NewData) =:= lookup_balance(To, Data) + Tx#transfer_tx.value
          end)).

prop_lock_tx_locks_account_or_extends_lock() ->
    ?FORALL(
       {{Data, _}, Tx=#lock_tx{locked_until=NewLockedUntil}},
       data_sks_and_lock_tx(),
       begin
           {#'ResponseDeliverTx'{code='OK'}, NewData} =
               committing(
                 #'RequestDeliverTx'{
                    tx=tx_serialize(Tx)},
                 Data),
           FreshAccount = get_account(from(Tx), NewData),
           FreshAccount#account.locked_until =:= NewLockedUntil
       end).

prop_account_tx_extends_validity() ->
    ?FORALL(
       {{Data, _}, Tx=#account_tx{to=ToAddress}},
       data_sks_and_account_tx(),
       begin
           {#'ResponseDeliverTx'{code='OK'}, NewData} =
               committing(
                 #'RequestDeliverTx'{
                    tx=tx_serialize(Tx)},
                 Data),
           FreshTo = get_account(ToAddress, NewData),
           FreshTo#account.valid_until =:= Tx#account_tx.valid_until
       end).

prop_vote_tx_changes_vote() ->
    ?FORALL(
       {Data, Tx},
       data_with_vote_tx(),
       begin
           {#'ResponseDeliverTx'{code='OK'}, NewData} =
               committing(
                 #'RequestDeliverTx'{
                    tx=tx_serialize(Tx)},
                 Data),
           get_vote(from(Tx), NewData) =:= Tx#vote_tx.vote
       end).

%% TODO: Unify vote_txs_hash and transfer_txs_hash.
prop_vote_tx_is_added_to_vote_txs_hash() ->
    ?FORALL(
       {Data, Tx},
       data_with_vote_tx(),
       begin
           #data{vote_txs_hash=TxsHash} = Data,
           TxBin = tx_serialize(Tx),
           {keep_state, #data{vote_txs_hash=NewTxsHash}, {reply, "from", #'ResponseDeliverTx'{code='OK'}}} =
               committing({call, "from"}, #'RequestDeliverTx'{tx=TxBin}, Data),
           NewTxsHash =:= ?HASH(<<TxsHash/binary, TxBin/binary>>)
       end).

prop_tx_puts_fee_into_fee_deposit() ->
    ?FORALL(
       {Data, Tx},
       data_with_tx(),
       begin
           #data{fee_deposit=FeeDeposit} = Data,
           Fee = fee(Tx, Data),
           {#'ResponseDeliverTx'{code='OK'}, #data{fee_deposit=NewFeeDeposit}} =
               committing(
                 #'RequestDeliverTx'{
                    tx=tx_serialize(Tx)},
                 Data),
           NewFeeDeposit =:= FeeDeposit + Fee
       end).

prop_tx_does_not_change_money_supply() ->
    ?FORALL(
       {Data, Tx},
       data_with_tx(),
       money_supply(apply_tx(Tx, Data)) =:= money_supply(Data)).

prop_invalid_tx_is_negatively_checked() ->
    ?FORALL(
       {Data, TxBin},
       data_with_invalid_tx_bin(),
       begin
           #'ResponseCheckTx'{code=ResponseCode} =
               gossiping(
                 #'RequestCheckTx'{
                    tx=TxBin},
                 Data),
           ResponseCode =/= 'OK'
       end).

prop_invalid_tx_does_not_modify_data() ->
    ?FORALL(
       {Data, TxBin},
       data_with_invalid_tx_bin(),
       begin
           {#'ResponseDeliverTx'{code=ResponseCode}, NewData} =
               committing(
                 #'RequestDeliverTx'{
                    tx=TxBin},
                 Data),
           ResponseCode =/= 'OK' andalso Data =:= NewData
       end).

prop_validators_drawing_at_endblock() ->
    ?FORALL(
       Data=#data{epoch_length=EpochLength, height=Height, validators=Validators},
       data(),
       begin
           {keep_state, #data{validators=NewValidators}, {reply, "from", #'ResponseEndBlock'{diffs=Diffs}}} =
               committing({call, "from"}, #'RequestEndBlock'{}, Data),
           case Height rem EpochLength of
               0 ->
                   %% TODO: Check that diffs are calculated properly.
                   NewValidators =:= new_validators(Data);
               _ ->
                   NewValidators =:= Validators andalso Diffs =:= []
           end
       end).

prop_info_responds_with_app_hash_and_height() ->
    ?FORALL(
       Data,
       data(),
       begin
           #'ResponseInfo'{last_block_height=Height, last_block_app_hash=AppHash} =
               gossiping(#'RequestInfo'{}, Data),
           Height =:= height(Data) andalso AppHash =:= app_hash(Data)
       end).

prop_commit_responds_with_app_hash() ->
    ?FORALL(
       Data,
       data(),
       begin
           {next_state, gossiping, NewData, {reply, "from", #'ResponseCommit'{data=ResponseData}}} =
               committing({call, "from"}, #'RequestCommit'{}, Data),
           ResponseData =:= app_hash(NewData)
       end).

prop_end_block_increments_height() ->
    ?FORALL(
       Data,
       data(),
       height(end_block(Data)) =:= height(Data) + 1).

prop_end_block_expires_accounts() ->
    ?FORALL(
       {Data, Account},
       data_with_account(),
       begin
           ExpiringAccount = Account#account{valid_until=Data#data.height},
           ExpiringData = put_account(ExpiringAccount, Data),
           NewData = end_block(ExpiringData),
           get_account(Account#account.address, NewData) =:= none
       end).

%% TODO: Give this more attention, ensure that the right thing is tested.
prop_after_end_block_all_validators_have_accounts() ->
    ?FORALL(
       {Data, Account},
       data_with_account(),
       begin
           ExpiringAccount = Account#account{valid_until=Data#data.height},
           ExpiringData = put_account(ExpiringAccount, Data),
           NewData = end_block(ExpiringData),
           get_account(Account#account.address, NewData) =:= none
       end).

prop_end_block_does_not_expire_valid_accounts() ->
    ?FORALL(
       {{Data, Account}, ValidFor},
       {data_with_account(), pos_integer()},
       begin
           NonExpiringAccount = Account#account{valid_until=Data#data.height + ValidFor},
           NonExpiringData = put_account(NonExpiringAccount, Data),
           NewData = end_block(NonExpiringData),
           get_account(Account#account.address, NewData) =/= none
       end).

%% TODO: Reconsider policy of fee deposit granting after ABCI introduces ways to handle validators that misbehave. Maybe fee deposits should be granted only at the end of each epoch?
%%
%% The property below had been failing, so it has been commented out.
%% prop_end_block_grants_fee_deposit_to_some_old_validator() ->
%%     ?FORALL(
%%        Data,
%%        data(),
%%        begin
%%            #data{fee_deposit=FeeDeposit} = Data,
%%            NewData = end_block(Data),
%%            gb_merkle_trees:foldr(
%%              fun ({Address, _}, OK) ->
%%                      #account{balance=Balance} = get_account(Address, NewData),
%%                      OK orelse
%%                          begin
%%                              case gb_merkle_trees:lookup(Address, NewData#data.validators) of
%%                                  none ->
%%                                      false;
%%                                  _ ->
%%                                      #account{balance=Balance} = get_account(Address, NewData),
%%                                      #account{balance=OldBalance} = get_account(Address, Data),
%%                                      Balance =:= FeeDeposit + OldBalance
%%                              end
%%                          end
%%              end,
%%              false,
%%              Data#data.validators)
%%        end).

prop_end_block_unlocks_some_accounts() ->
    ?FORALL(
       {{Data=#data{height=Height}, _}, Account=#account{locked_until=LockedUntil, valid_until=ValidUntil}},
       data_sks_and_account(locked),
       ?IMPLIES(
          ValidUntil > Height,
          begin
              NewData = end_block(Data),
              #account{locked_until=NewLockedUntil} = get_account(Account#account.address, NewData),
              NewLockedUntil =:=
                  case LockedUntil of
                      Height ->
                          none;
                      _ ->
                          LockedUntil
                  end
          end)).

prop_end_block_truncates_fresh_txs() ->
    ?FORALL(
       Data,
       data(),
       begin
           FreshTxs = Data#data.fresh_txs,
           #data{fresh_txs=NewFreshTxs, height=NewHeight} = end_block(Data),
           NewFreshTxs =:= ?SETS:filter(fun ({ValidSince, _Tx}) -> ValidSince >= NewHeight - Data#data.epoch_length + 2 end, FreshTxs)
       end).

prop_transfer_tx_and_vote_tx_is_added_to_fresh_txs() ->
    ?FORALL(
       {Data, Tx},
       oneof(
         [fun data_with_transfer_tx/0,
          fun data_with_vote_tx/0]),
       begin
           TxBin = tx_serialize(Tx),
           {keep_state, #data{fresh_txs=NewFreshTxs}, {reply, "from", #'ResponseDeliverTx'{code='OK'}}} =
               committing({call, "from"}, #'RequestDeliverTx'{tx=TxBin}, Data),
           ?SETS:is_element({get_valid_since(Tx), ?HASH(TxBin)}, NewFreshTxs)
       end).

prop_transfer_tx_is_added_to_transfer_txs_hash() ->
    ?FORALL(
       {Data, Tx},
       data_with_transfer_tx(),
       begin
           #data{transfer_txs_hash=TransferTxsHash} = Data,
           TxBin = tx_serialize(Tx),
           {keep_state, #data{transfer_txs_hash=NewTransferTxsHash}, {reply, "from", #'ResponseDeliverTx'{code='OK'}}} =
               committing({call, "from"}, #'RequestDeliverTx'{tx=TxBin}, Data),
           NewTransferTxsHash =:= ?HASH(<<TransferTxsHash/binary, TxBin/binary>>)
       end).
-endif.
