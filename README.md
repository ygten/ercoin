# Ercoin

Ercoin is a proof of stake cryptocurrency written in [Erlang](https://www.erlang.org) using [Tendermint](https://tendermint.com) and released under Apache License 2.0. Currently in development phase; some features below may be unimplemented yet and are subject to change.

[Issues are tracked on GitLab.](https://gitlab.com/Ercoin/ercoin/issues) Contributions are welcome. Before submitting a patch, read [contributing guidelines](CONTRIBUTING.md).

For support and other ephemeral discussions, see [the #ercoin IRC channel on irc.freenode.net](irc://irc.freenode.net/ercoin).

## Features

* 100% proof of stake — no wasteful mining.
* Transaction messages.
* 1-second confirmations.
* No chain forks.
* Expirable accounts instead of unspent transaction outputs.
* Fair distribution via proof of burn — no premine, no ICO.
* Accounts managed by multiple keys in arbitrary combinations.

## Development installation

1. Install [Tendermint](https://tendermint.com) (version 0.10.x).
2. Install [Erlang](https://www.erlang.org) (19 is the minimum version).
3. Install [IPFS](https://ipfs.io/) and run `ipfs init`.
4. Install [Sodium](https://download.libsodium.org/doc/).
5. Clone the Ercoin’s repository and enter the created directory.
6. `./bootstrap.sh`.
7. `make app shell`.
8. In the opened Erlang shell, execute `application:ensure_all_started(ercoin).`.
9. In another window, run `tendermint node --home ~/.ercoin/`.

You can play with the node using [`ercoin_wallet`](https://gitlab.com/Ercoin/ercoin_wallet).

## Technical specification

All numbers are big endian.

Keys are Ed25519.

### Transfer transaction format

* Transaction type (0, 1 byte).
* Valid since (4 bytes) — earliest block height at which tx can be included.
* From address (32 bytes).
* To address (32 bytes).
* Value (8 bytes).
* Message length (1 byte).
* Message.
* Signature (64 bytes) — all the previous fields signed.

Fee: per tx + per byte

### Account transaction format

* Transaction type (1, 1 byte).
* Valid until block (4 bytes).
* From address (32 bytes).
* To address (32 bytes).
* Signature (64 bytes) — all the previous fields signed.

Fee: Per tx + per byte + per blocks.

### Lock transaction format

* Transaction type (2, 1 byte).
* Locked until block (4 bytes).
* Address (32 bytes).
* Signature (64 bytes) — all the previous fields signed.

Fee: per tx + per byte + per blocks.

### Vote transaction format

* Transaction type (3, 1 byte).
* Valid since (4 bytes) — earliest block height at which tx can be included.
* Address (32 bytes).
* Votes.
* Signature (64 bytes) — all the previous fields signed.

Fee: none

### Votes format

* Price per tx (8 bytes).
* Price per 256 bytes per tx (8 bytes).
* Price of storing an account for 1 day (8 bytes).
* Protocol version (1 byte).

### Account balances

Key:
* public key (32 bytes).

Value:

* Paid until (4 bytes).
* Balance (8 bytes).
* Locked until (optional, 4 bytes). May not be lower than “paid until”.

### Application hash

The following Merkle tree is formed when calculating an application hash:

* Accounts’ root hash.
* Other data hash:
  * Protocol number.
  * Block height.
  * Validators’ hash.
  * Transfer transactions’ hash.
  * Vote transactions’ hash.

### State machine mechanism

Blocks are divided into epochs, during which validator set is almost immutable.

Validators are responsible for providing replay protection.

Transfer transactions modify accounts’ balances. They may not be performed from a locked account, but they can transfer funds to a locked account.

Lock transactions lock accounts and declare them as ready to become a validator. They may be performed by locked accounts.

Vote transactions can be performed by validators to vote for fee sizes and protocol changes.

Account transaction create accounts or extend their validity.

Every transaction except a vote transaction deducts a fee from the account from which it is performed. Fees are calculated from validators’ votes.

At the end of every epoch, a new validator set is drawn from the locked accounts. Voting power is statistically proportional to account balance and to lock period.

If a validator proposes an invalid tx in a block, it pays a fee like it was a transaction tx.

If balance of a validator reaches 0, it is removed from the validator set.

## Initial distribution

Initial amount of coins will be distributed to addresses proportionally to the associated amounts of [BlackCoin](https://blackcoin.co) burnt in a selected time window using [`OP_RETURN`](https://en.bitcoin.it/wiki/OP_RETURN).

## License

This software is licensed under under [the Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) (the “License”); you may not use this software except in compliance with the License. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.
